#   import  string
#   alphabet = list(string.ascii_letters)
#   s = str(input("Input your text: ")).lower()
#   for i in set(s):
#       if i in alphabet:
#           print(i, ' - ', s.count(i)/len(s))

import string

alphabet = list(string.ascii_letters)
message = str(input("Input your message: ")).upper()
for i in range(len(message)):
    if i == message.index(message[i]) and message[i] in alphabet:
        l = []
        l.append("Частота появи символу " + str(message[i]) + " - " + str(float("{:.3f}".format((message.count(message[i])/len(message))))))
        print("\n".join(l))